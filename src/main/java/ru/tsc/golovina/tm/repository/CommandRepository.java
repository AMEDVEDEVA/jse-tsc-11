package ru.tsc.golovina.tm.repository;

import ru.tsc.golovina.tm.api.repository.ICommandRepository;
import ru.tsc.golovina.tm.constant.ArgumentConst;
import ru.tsc.golovina.tm.constant.TerminalConst;
import ru.tsc.golovina.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO, "Display system information"
    );

    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP, "Display list of terminal commands"
    );

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT, "Display developer info"
    );

    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION, "Display program version"
    );

    public static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS, "Display list of commands"
    );

    public static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "Display list of arguments"
    );

    public static final Command EXIT = new Command(
            TerminalConst.EXIT, null, "Close application"
    );

    public static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null, "Display list of projects"
    );

    public static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null, "Create new project"
    );

    public static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null, "Drop all projects"
    );

    public static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null, "Display list of tasks"
    );

    public static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null, "Create new task"
    );

    public static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null, "Drop all tasks"
    );

    public static final Command PROJECT_SHOW_BY_ID = new Command(
            TerminalConst.PROJECT_SHOW_BY_ID, null, "Show project by id"
    );

    public static final Command PROJECT_SHOW_BY_INDEX = new Command(
            TerminalConst.PROJECT_SHOW_BY_INDEX, null, "Show project by index"
    );

    public static final Command PROJECT_SHOW_BY_NAME = new Command(
            TerminalConst.PROJECT_SHOW_BY_NAME, null, "Show project by name"
    );

    public static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID, null, "Remove project by id"
    );

    public static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.PROJECT_REMOVE_BY_INDEX, null, "Remove project by index"
    );

    public static final Command PROJECT_REMOVE_BY_NAME = new Command(
            TerminalConst.PROJECT_REMOVE_BY_NAME, null, "Remove project by name"
    );

    public static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.PROJECT_UPDATE_BY_ID, null, "Update project by id"
    );

    public static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.PROJECT_UPDATE_BY_INDEX, null, "Update project by index"
    );

    public static final Command TASK_SHOW_BY_ID = new Command(
            TerminalConst.TASK_SHOW_BY_ID, null, "Show project by id"
    );

    public static final Command TASK_SHOW_BY_INDEX = new Command(
            TerminalConst.TASK_SHOW_BY_INDEX, null, "Show project by index"
    );

    public static final Command TASK_SHOW_BY_NAME = new Command(
            TerminalConst.TASK_SHOW_BY_NAME, null, "Show project by name"
    );

    public static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.TASK_REMOVE_BY_ID, null, "Remove project by id"
    );

    public static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.TASK_REMOVE_BY_INDEX, null, "Remove project by index"
    );

    public static final Command TASK_REMOVE_BY_NAME = new Command(
            TerminalConst.TASK_REMOVE_BY_NAME, null, "Remove project by name"
    );

    public static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.TASK_UPDATE_BY_ID, null, "Update project by id"
    );

    public static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.TASK_UPDATE_BY_INDEX, null, "Update project by index"
    );

    public static final Command[] TERMINAL_COMMANDS = new Command[]{
            INFO, ABOUT, HELP, VERSION, COMMANDS, ARGUMENTS, PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            TASK_LIST, TASK_CREATE, TASK_CLEAR, TASK_SHOW_BY_ID, TASK_SHOW_BY_INDEX, TASK_SHOW_BY_NAME,
            TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX, TASK_REMOVE_BY_NAME, TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            PROJECT_SHOW_BY_ID, PROJECT_SHOW_BY_INDEX, PROJECT_SHOW_BY_NAME, PROJECT_REMOVE_BY_ID,
            PROJECT_REMOVE_BY_INDEX, PROJECT_REMOVE_BY_NAME, PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX, EXIT
    };

    public Command[] getCommands() {
        return TERMINAL_COMMANDS;
    }

}
