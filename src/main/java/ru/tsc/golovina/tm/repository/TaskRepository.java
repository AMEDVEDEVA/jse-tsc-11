package ru.tsc.golovina.tm.repository;

import ru.tsc.golovina.tm.api.repository.ITaskRepository;
import ru.tsc.golovina.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final Task task) {
        tasks.add(task);
    }

    @Override
    public void remove(final Task task) {
        tasks.remove(task);
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public boolean existsById(final String id) {
        return findById(id) != null;
    }

    @Override
    public boolean existsByIndex(final int index) {
        return index < tasks.size() && index >= 0;
    }

    @Override
    public boolean existsByName(final String name) {
        return findByName(name) != null;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public Task findById(final String id) {
        for (Task task : tasks)
            if (id.equals(task.getId())) return task;
        return null;
    }

    @Override
    public Task findByName(final String name) {
        for (Task task : tasks)
            if (name.equals(task.getName())) return task;
        return null;
    }

    @Override
    public Task findByIndex(final int index) {
        return tasks.get(index);
    }

    @Override
    public Task removeById(final String id) {
        final Task task = findById(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final int index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

}
