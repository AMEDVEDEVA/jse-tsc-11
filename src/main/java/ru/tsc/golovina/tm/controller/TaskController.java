package ru.tsc.golovina.tm.controller;

import ru.tsc.golovina.tm.api.controller.ITaskController;
import ru.tsc.golovina.tm.api.service.ITaskService;
import ru.tsc.golovina.tm.model.Task;
import ru.tsc.golovina.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        System.out.println("[LIST TASKS]");
        final List<Task> tasks = taskService.findAll();
        if (tasks.size() <= 0) {
            System.out.println("Task list is empty now");
            return;
        }
        for (Task task : tasks)
            showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        taskService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void showTask(Task task) {
        System.out.println(task);
    }

    @Override
    public void showById() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findById(id);
        if (task == null) {
            System.out.println("Incorrect values");
            return;
        }
        showTask(task);
    }

    @Override
    public void showByName() {
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findByName(name);
        if (task == null) {
            System.out.println("Incorrect values");
            return;
        }
        showTask(task);
    }

    @Override
    public void showByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findByIndex(index);
        if (task == null) {
            System.out.println("Incorrect values");
            return;
        }
        showTask(task);
    }

    @Override
    public void updateByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (!taskService.existsByIndex(index)) {
            System.out.println("Incorrect values");
            return;
        }
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        taskService.updateByIndex(index, name, description);
        System.out.println("[OK]");
    }

    @Override
    public void updateById() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        if (!taskService.existsById(id)) {
            System.out.println("Incorrect values");
            return;
        }
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        taskService.updateById(id, name, description);
        System.out.println("[OK]");
    }

    @Override
    public void removeById() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        if (!taskService.existsById(id)) {
            System.out.println("Incorrect values");
            return;
        }
        taskService.removeById(id);
        System.out.println("[OK]");
    }

    @Override
    public void removeByName() {
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        if (!taskService.existsByName(name)) {
            System.out.println("Incorrect values");
            return;
        }
        taskService.removeByName(name);
        System.out.println("[OK]");
    }

    @Override
    public void removeByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (!taskService.existsByIndex(index)) {
            System.out.println("Incorrect values");
            return;
        }
        taskService.removeByIndex(index);
        System.out.println("[OK]");
    }

}
