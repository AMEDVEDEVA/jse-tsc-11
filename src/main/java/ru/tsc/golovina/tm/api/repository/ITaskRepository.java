package ru.tsc.golovina.tm.api.repository;

import ru.tsc.golovina.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    boolean existsById(String id);

    boolean existsByIndex(int index);

    boolean existsByName(String name);

    void add(Task task);

    void remove(Task task);

    void clear();

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(int index);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(int index);

}
