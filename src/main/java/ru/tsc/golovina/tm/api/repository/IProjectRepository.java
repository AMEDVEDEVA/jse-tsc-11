package ru.tsc.golovina.tm.api.repository;

import ru.tsc.golovina.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    boolean existsById(String id);

    boolean existsByIndex(int index);

    boolean existsByName(String name);

    void add(Project project);

    void remove(Project project);

    void clear();

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(int index);

    Project removeById(String id);

    Project removeByName(String name);

    Project removeByIndex(int index);

}
