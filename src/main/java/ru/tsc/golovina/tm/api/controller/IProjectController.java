package ru.tsc.golovina.tm.api.controller;

import ru.tsc.golovina.tm.model.Project;

public interface IProjectController {

    void showProjects();

    void showProject(Project project);

    void clearProjects();

    void createProject();

    void showById();

    void showByName();

    void showByIndex();

    void updateByIndex();

    void updateById();

    void removeById();

    void removeByName();

    void removeByIndex();

}
